We are going to cover the topics given below

- Basic setup
- MathApp using calculator Service
- Stock Portfolio App
- Mockito with Junit
- Mockito standalone
- Mockito with TestNG
- Mockito with annotation
- Mock vs Spy
- Usage of doCallRealMethod()
- Static, private, final method call using powermockito
- Why we cant use annotaion in powermockito?
- Go through MBTWebElementTest class