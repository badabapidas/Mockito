package test;

import java.util.HashSet;
import java.util.Set;

public class Duplicate {

	public static void main(String args[]) {
		int arr[] = { 1, 4, 3, 6, 5 };

		// System.out.println(findDup(arr));
		System.out.println(findEffDuplicate(arr));
	}

	public static Boolean findDup(int arr[]) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					count++;
				}
			}
		}
		if (count > 0) {
			return true;

		} else
			return false;
	}

	public static boolean findEffDuplicate(int[] array) {
		
		Set<Integer> set = new HashSet<>();
		for (int arrayI : array) {
			set.add(arrayI);
		}
		if (set.size() != array.length) {
			return true;
		}
		return false;
	}

}
