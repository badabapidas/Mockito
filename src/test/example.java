package test;

public class example {
	public static void main(String[] args) {
		Animal dog = new Dog();
		Animal cat = new Cat();
		dog.bark();
		cat.bark();
	}

}

class Animal {

	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void bark() {
		System.out.println("All animal can't bark");
	}
}

class Dog extends Animal {
	public void bark() {
		System.out.println("Bho bho");
	}
}

class Cat extends Animal {
	public void bark() {
		System.out.println("meo meo");
	}
}
