/**
 * Test class to demonstrate how to write test using testng and mockito annotations
 */

package mockito.test.testng;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import mockito.api.Portfolio;
import mockito.api.Stock;
import mockito.api.interfaces.StockService;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class PortfolioTestWithAnnotation {

	@Mock
	StockService stockServiceMock;

	@InjectMocks
	@Spy
	Portfolio portfolio;

	List<Stock> stocksMock;

	@BeforeClass
	private void initializeMocks() {
		MockitoAnnotations.initMocks(this);
		stocksMock = new ArrayList<>();
		Stock googleStock = new Stock("1", "Google", 10);
		Stock microsoftStock = new Stock("2", "Microsoft", 100);
		stocksMock.add(googleStock);
		stocksMock.add(microsoftStock);
		portfolio.setStocks(stocksMock);

	}

	@Test
	public void testMarketValue() throws Exception {

		// mock constructor
		// PowerMockito.whenNew(Stock.class).withArguments("1", "Google",
		// 10).thenReturn(googleStock);
		// PowerMockito.whenNew(Stock.class).withArguments("2", "Microsoft",
		// 100).thenReturn(MicrosoftStock);

		when(stockServiceMock.getPrice(stocksMock.get(0))).thenReturn(50.00);
		when(stockServiceMock.getPrice(stocksMock.get(1))).thenReturn(1000.00);

		assertEquals(100500.0, portfolio.getMarketValue());
	}

}
