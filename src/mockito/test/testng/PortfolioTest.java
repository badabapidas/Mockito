/**
 * Test class to demonstrate how to write test using testng and mockito with out annotations
 */

package mockito.test.testng;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import mockito.api.Portfolio;
import mockito.api.Stock;
import mockito.api.interfaces.StockService;

public class PortfolioTest {

	StockService stockServiceMock;
	Portfolio portfolio;
	List<Stock> stocksMock;

	@BeforeClass
	private void initializeMocks() {
		initializeMocksAndSpys();
		initializeStocks();
	}

	/**
	 * Method to set up test environment by creating mocks and spyers
	 */
	private void initializeMocksAndSpys() {
		stockServiceMock = PowerMockito.mock(StockService.class);
		// portfolio = PowerMockito.spy(new Portfolio());

		// Mock portfolio class for do a real call instead of spy
		portfolio = PowerMockito.mock(Portfolio.class);

		portfolio.setStockService(stockServiceMock);

	}

	/**
	 * Method to initialize stocks
	 */
	private void initializeStocks() {
		stocksMock = new ArrayList<>();
		Stock googleStock = new Stock("1", "Google", 10);
		Stock microsoftStock = new Stock("2", "Microsoft", 100);
		stocksMock.add(googleStock);
		stocksMock.add(microsoftStock);
		portfolio.setStocks(stocksMock);
	}

	@Test
	public void testMarketValue() throws Exception {

		when(stockServiceMock.getPrice(stocksMock.get(0))).thenReturn(50.00);
		when(stockServiceMock.getPrice(stocksMock.get(1))).thenReturn(1000.00);

		// assertEquals(100500.0, portfolio.getMarketValue());

		// here we are doing a real call on a mocked object
		Mockito.doCallRealMethod().when(portfolio).getMarketValue();

	}

}
