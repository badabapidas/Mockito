/**
 * Test class to demonstrate how to write test using testng and mockito with out annotations
 */

package mockito.test.testng;

import java.util.List;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import mockito.api.MockitoProperties;
import mockito.api.Portfolio;
import mockito.api.Stock;
import mockito.api.interfaces.StockService;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@PrepareForTest({ MockitoProperties.class, Portfolio.class })
public class PowerMockitoTest extends PowerMockTestCase {

	StockService stockServiceMock;
	Portfolio portfolio;

	@BeforeMethod
	private void initializeMocks() {
		initializeMocksAndSpys();
	}

	/**
	 * Method to set up test environment by creating mocks and spyers
	 */
	private void initializeMocksAndSpys() {
		stockServiceMock = PowerMockito.mock(StockService.class);
		portfolio = PowerMockito.spy(new Portfolio());
		portfolio.setStockService(stockServiceMock);

	}

	/**
	 * Static method call
	 */
	@Test
	public void testStaticApi() {
		PowerMockito.mockStatic(MockitoProperties.class);
		when(MockitoProperties.isMockSet()).thenReturn(true);
		assertTrue(portfolio.getStaticValue());
	}

	/**
	 * Final method call
	 */
	@Test
	public void testFinalApi() {
		when(portfolio.getFinalMethod()).thenReturn(true);
		assertTrue(portfolio.getFinalMethod());

	}

	/**
	 * Private method call
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPrivateApi() throws Exception {
		PowerMockito.doReturn(true).when(portfolio, "getPrivateMethod");

		assertTrue(portfolio.callPrivateMethod());

		// Verify private method invoked once
		PowerMockito.verifyPrivate(portfolio, Mockito.times(1)).invoke("getPrivateMethod");

	}
}
