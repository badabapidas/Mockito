package mockito.test.junit;

import static org.mockito.Mockito.when;

import javax.management.relation.RelationServiceNotRegisteredException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import mockito.api.MathApp;
import mockito.api.interfaces.CalculatorService;

import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

// @RunWith attaches a runner with the test class to initialize the test data
@RunWith(MockitoJUnitRunner.class)
public class MathAppTest {

	// @InjectMocks annotation is used to create and inject the mock object
	@InjectMocks
	MathApp mathApplication = new MathApp();

	// @Mock annotation is used to create the mock object to be injected
	@Mock
	CalculatorService calcService;

	@Test
	public void testAdd() {
		// add the behavior of calc service to add two numbers
		when(calcService.add(10.0, 20.0)).thenReturn(30.00);

		// add the behavior of calc service to subtract two numbers
		when(calcService.subtract(20.0, 10.0)).thenReturn(10.00);

		// test the add functionality
		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);
		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);
		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);

		// test the subtract functionality
		Assert.assertEquals(mathApplication.subtract(20.0, 10.0), 10.0, 0.0);

		// default call count is 1
		verify(calcService).subtract(20.0, 10.0);

		// check if add function is called three times
		verify(calcService, times(3)).add(10.0, 20.0);

		// verify that method was never called on a mock
		verify(calcService, never()).multiply(10.0, 20.0);

		// check a minimum 1 call count
		verify(calcService, atLeastOnce()).subtract(20.0, 10.0);

		// check if add function is called minimum 2 times
		verify(calcService, atLeast(2)).add(10.0, 20.0);

		// check if add function is called maximum 3 times
		verify(calcService, atMost(3)).add(10.0, 20.0);

	}

	@Test(expected = RuntimeException.class)
	public void testException() {
		// add the behavior to throw exception
		doThrow(new RuntimeException("Add operation not	 implemented")).when(calcService).add(10.0, 20.0);

		// test the add functionality
		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);
	}

	@Test
	public void testAddAndSubtract_ExecutionOrder() {

		// add the behavior to add numbers
		when(calcService.add(20.0, 10.0)).thenReturn(30.0);

		// subtract the behavior to subtract numbers
		when(calcService.subtract(20.0, 10.0)).thenReturn(10.0);

		// test the add functionality
		Assert.assertEquals(mathApplication.add(20.0, 10.0), 30.0, 0);

		// test the subtract functionality
		Assert.assertEquals(mathApplication.subtract(20.0, 10.0), 10.0, 0);

		// create an inOrder verifier for a single mock
		InOrder inOrder = inOrder(calcService);

		// following will make sure that add is first called then subtract is
		// called.
		// inOrder.verify(calcService).subtract(20.0, 10.0);
		inOrder.verify(calcService).add(20.0, 10.0);
		inOrder.verify(calcService).subtract(20.0, 10.0);

	}

	@Test
	public void testCallBack() {

		when(calcService.add(10.0, 20.0)).then(new Answer<Double>() {

			@Override
			public Double answer(InvocationOnMock invocation) throws Throwable {
				// get the arguments passed to mock
				Object[] args = invocation.getArguments();

				// get the mock
				Object mock = invocation.getMock();

				// return the result
				// return 30.0;

				Double value = 0.0;
				for (Object object : args) {
					value += (Double) object;
				}
				return value;

			}
		});

		// test the add functionality
		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);

	}

	@Test
	public void testResetMode() {
		// Mockito provides the capability to a reset a mock so that it can be
		// reused later. Take a look at the following code snippet.

		// add the behavior to add numbers
		when(calcService.add(20.0, 10.0)).thenReturn(30.0);

		// test the add functionality
		Assert.assertEquals(mathApplication.add(20.0, 10.0), 30.0, 0);

		// reset the mock
		reset(calcService);

		// test the add functionality after resetting the mock
		Assert.assertNotEquals(mathApplication.add(20.0, 10.0), 30.0, 0);
	}

	@Test
	public void testBDD() {
		/**
		 * Behavior Driven Development is a style of writing tests uses given,
		 * when and then format as test methods. Mockito provides special
		 * methods to do so. Take a look at the following code snippet.
		 */

		// Given
		given(calcService.add(20.0, 10.0)).willReturn(30.0);

		// when
		double result = calcService.add(20.0, 10.0);

		// then
		Assert.assertEquals(result, 30.0, 0);
	}

	@Test
	public void testSpy() {
		Calculator calculator = new Calculator();
		calcService = spy(calculator);
		mathApplication.setCalculatorService(calcService);

		// perform operation on real object
		// test the add functionality
		Assert.assertEquals(mathApplication.add(20.0, 10.0), 30.0, 0);
	}

	class Calculator implements CalculatorService {
		@Override
		public double add(double input1, double input2) {
			return input1 + input2;
		}

		@Override
		public double subtract(double input1, double input2) {
			throw new UnsupportedOperationException("Method not implemented yet!");
		}

		@Override
		public double multiply(double input1, double input2) {
			throw new UnsupportedOperationException("Method not implemented yet!");
		}

		@Override
		public double divide(double input1, double input2) {
			throw new UnsupportedOperationException("Method not implemented yet!");
		}
	}

	@Test
	public void testTimeout() {
		/**
		 * Mockito provides a special Timeout option to test if a method is
		 * called within stipulated time frame.
		 */

		// add the behavior to add numbers
		when(calcService.add(20.0, 10.0)).thenReturn(30.0);

		// subtract the behavior to subtract numbers
		when(calcService.subtract(20.0, 10.0)).thenReturn(10.0);

		// test the subtract functionality
		Assert.assertEquals(mathApplication.subtract(20.0, 10.0), 10.0, 0);

		// test the add functionality
		Assert.assertEquals(mathApplication.add(20.0, 10.0), 30.0, 0);

		// Allows verifying with timeout. It causes a verify to wait for a specified period of time for a desired interaction rather than fails immediately if has not already happened
		verify(calcService, timeout(100)).add(20.0, 10.0);

		// invocation count can be added to ensure multiplication invocations
		// can be checked within given timeframe
		verify(calcService, timeout(100).times(1)).subtract(20.0, 10.0);
	}

	@Test
	public void testAnotherAdd() {
		calcService = new CalculatorService() {

			@Override
			public double subtract(double input1, double input2) {
				return 0;
			}

			@Override
			public double multiply(double input1, double input2) {
				return 0;
			}

			@Override
			public double divide(double input1, double input2) {
				return 0;
			}

			@Override
			public double add(double input1, double input2) {
				return input1 + input2;
			}
		};
		System.out.println(calcService.add(20.0, 30.0));

		Assert.assertEquals(calcService.add(20.0, 30.0), 50.0, 0);
	}

}