/**
 * A stock service returns the current price of a stock.
 */
package mockito.api.interfaces;

import mockito.api.Stock;

public interface StockService {
	public double getPrice(Stock stock);
}