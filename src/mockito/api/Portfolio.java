/**
 * An object to carry a list of stocks and to get the market value computed using stock prices and stock quantity.
 */

package mockito.api;

import java.util.List;

import mockito.api.interfaces.StockService;

public class Portfolio {
	private StockService stockService;
	private List<Stock> stocks;

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public double getMarketValue() {
		double marketValue = 0.0;

		for (Stock stock : stocks) {
			marketValue += stockService.getPrice(stock) * stock.getQuantity();
		}
		return marketValue;
	}

	/**
	 * Dummy method to test static
	 * 
	 * @return
	 */
	public boolean getStaticValue() {
		if (MockitoProperties.isMockSet()) {
			return true;
		}
		return false;
	}

	/**
	 * Dummy method to test final
	 * 
	 * @return
	 */
	public final boolean getFinalMethod() {
		return false;
	}

	/**
	 * Dummy method to test private
	 * 
	 * @return
	 */
	private boolean getPrivateMethod() {
		return false;
	}

	public boolean callPrivateMethod() {
		return getPrivateMethod();
	}

}