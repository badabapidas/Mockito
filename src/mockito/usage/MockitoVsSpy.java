package mockito.usage;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;
import org.testng.annotations.Test;

public class MockitoVsSpy {

	@Test
	public void whenCreateMock_thenCreated() {
		List<String> mockedList = Mockito.mock(ArrayList.class);
		mockedList.add("one");
		Mockito.verify(mockedList).add("one");
		assertEquals(0, mockedList.size());
	}

	@Test
	public void spyMethod() {
		List<String> spyList = Mockito.spy(new ArrayList<>());
		spyList.add("one");
		Mockito.verify(spyList).add("one");
		assertEquals(1, spyList.size());

	}

	@Test
	public void getSizeMock() {
		List<String> mockedList = Mockito.mock(ArrayList.class);
		Mockito.when(mockedList.size()).thenReturn(10);

		assertEquals(mockedList.size(), 10);
	}
}
